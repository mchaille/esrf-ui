import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-jsonschema-form';

export default class LoginForm extends Component {
  login(formData) {
    this.props.login(formData.username, formData.password);
  }

  render() {
    if (!this.props.schema) {
      return (<div>Error no schema defined</div>);
    }

    return (
      <div className="login">
        <div className="login-inner">
          <Card>
            <Card.Header>Login</Card.Header>
            <Form
              schema={this.props.schema.definitions.LoginSchema}
              uiSchema={this.props.uiSchema}
              onSubmit={(e) => {
                this.login(e.formData);
              }}
              showErrorList={false}
            >
              <div>
                <Button className="pull-right" variant="info" type="submit">Login</Button>
              </div>
            </Form>
          </Card>
        </div>
      </div>
    );
  }
}

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
  schema: PropTypes.PropTypes.shape({}).isRequired,
  uiSchema: PropTypes.shape({
    uiSchema: PropTypes.shape({
    })
  })
};

LoginForm.defaultProps = {
  uiSchema: {
    'ui:order': ['username', 'password', 'client'],
    username: {
      'ui:placeholder': 'Username',
      'ui:title': 'Username'
    },
    password: {
      'ui:widget': 'password',
      'ui:placeholder': 'Password',
      'ui:title': 'Password'
    },
    client: {
      'ui:widget': 'hidden'
    }
  }
};
